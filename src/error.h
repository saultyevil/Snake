/* ***************************************************************************
 *
 * @file error.c
 *
 * @author E. J. Parkinson
 *
 * @date 12 Dec 2018
 *
 * @brief Definition for errors in the program.
 *
 * @details
 *
 * ************************************************************************** */

#define FILE_OPEN_ERR 1
#define FILE_CLOSE_ERR 2
#define MEM_ALLOC_ERR 3
#define TABLE_BOUNDS 4
#define UNKNOWN_PARAMETER 5
#define NO_INPUT 6
#define PAR_FILE_SYNTAX_ERR 7
#define FILE_IN_ERR 8
#define UNKNOWN_MODE 9
#define INVALID_VALUE 10
#define INVALID_TABLE 11